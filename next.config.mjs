/** @type {import('next').NextConfig} */
const nextConfig = {
      output: "export",
      assetPrefix: process.env.NODE_ENV === "production" ? "/number-game": '',
};

export default nextConfig;
