// page.tsx

import Home from "./Home";
import Footer from "./Footer";

export default function HomePage() {
  return (
    <div>
      <Home />
      <Footer />
    </div>
  );
}
