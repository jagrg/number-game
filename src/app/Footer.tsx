// Footer.tsx

import styles from "./page.module.css";

export default function Footer() {
  return (
    <footer>
      <div className={styles.footer}>
        <a href="https://git.sr.ht/~jagrg/number-game">Source code</a>
      </div>
    </footer>
  );
}
