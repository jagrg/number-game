// Home.tsx

"use client";
import { useState, useEffect } from "react";
import styles from "./page.module.css";
import Confetti from "react-confetti";

export default function Home() {
  const [randomNumber, setRandomNumber] = useState(generateRandomNumber());
  const [guess, setGuess] = useState(1);
  const [feedback, setFeedback] = useState("");
  const [showConfetti, setShowConfetti] = useState(false);

  function generateRandomNumber() {
    return Math.floor(Math.random() * 11);
  }

  function handleBoxClick(clickedNumber: number) {
    if (clickedNumber === randomNumber) {
      // Make the reward harder to get
      if (guess <= 3) {
        setShowConfetti(true);
      }
      if (guess <= 1) {
        setFeedback(
          `🎉 ${randomNumber} is correct! You guessed it in ${guess} attempt. Impressive!`,
        );
      } else {
        setFeedback(
          `🎉 ${randomNumber} is correct! You guessed it in ${guess} attempts`,
        );
      }
    } else if (clickedNumber > randomNumber) {
      setGuess(guess + 1);
      setFeedback("👈 Lower!");
    } else {
      setGuess(guess + 1);
      setFeedback("👉 Higher!");
    }

    // Select all box elements
    const boxElements = document.querySelectorAll(`.${styles.box}`);

    // Determine which boxes to color
    boxElements.forEach((box, index) => {
      if (index !== randomNumber) {
        if (clickedNumber >= randomNumber && index >= clickedNumber) {
          box.classList.add(styles.disabled);
        } else {
          if (clickedNumber <= randomNumber && index <= clickedNumber) {
            box.classList.add(styles.disabled);
          }
        }
      }
    });
  }

  function handleButtonClick() {
    setRandomNumber(generateRandomNumber());
    setGuess(1);
    setFeedback("");
    setShowConfetti(false);
    // Select all box elements
    const boxElements = document.querySelectorAll(`.${styles.box}`);

    // Remove the disabled class from all box elements
    boxElements.forEach((box) => {
      box.classList.remove(styles.disabled);
    });
  }

  // useEffect(() => {
  //   console.log(randomNumber);
  // }, [randomNumber]);

  // https://www.npmjs.com/package/react-confetti
  return (
    <main>
      {showConfetti && <Confetti numberOfPieces={80} />}
      <div className={styles.main}>
        <h1 className={styles.title}>
          Guess the <span className={styles.highlight}>number</span>
        </h1>
        <div className={styles.feedback}> {feedback}</div>
        <div className={styles.container}>
          {[...Array(11).keys()].map((number) => (
            <div
              className={styles.box}
              key={number}
              onClick={() => handleBoxClick(number)}
            >
              {number}
            </div>
          ))}
        </div>
        <button className={styles.button} onClick={handleButtonClick}>
          New Game
        </button>
      </div>
    </main>
  );
}
