import type { Metadata } from "next";
import "./globals.css";

export const metadata: Metadata = {
  title: "Number Guessing Game",
  description: "A guess number game with feedback and animation",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
